package com.fpdecuzzi.mutant.checker.model;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

public class MutantCheckerTest {

    private final MutantChecker mutantChecker;

    public MutantCheckerTest() {
        this.mutantChecker = new MutantChecker();
    }

    @Test
    public void isMutantByConsecutiveCHorizontal() throws IOException {
        String[] dnaLines = readDnaLines("/four-consecutive-C-horizontal.dna");
        assert mutantChecker.isMutant(dnaLines);
    }
    @Test
    public void isMutantByConsecutiveADiagonal() throws IOException {
        String[] dnaLines = readDnaLines("/four-consecutive-A-diagonal.dna");
        assert mutantChecker.isMutant(dnaLines);
    }
    @Test
    public void isMutantByConsecutiveGVertical() throws IOException {
        String[] dnaLines = readDnaLines("/four-consecutive-G-vertical.dna");
        assert mutantChecker.isMutant(dnaLines);
    }
    @Test
    public void notIsMutant() throws IOException {
        String[] dnaLines = readDnaLines("/no-consecutive-base.dna");
        assert !mutantChecker.isMutant(dnaLines);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwsIllegalArgumentExceptionBecauseInvalidCharacter() throws IOException {
        String[] dnaLines = readDnaLines("/invalid-character-has-k.dna");
        mutantChecker.isMutant(dnaLines);
    }
    @Test(expected = IllegalArgumentException.class)
    public void throwsIllegalArgumentExceptionBecauseNotSquare() throws IOException {
        String[] dnaLines = readDnaLines("/invalid-no-square.dna");
        assert mutantChecker.isMutant(dnaLines);
    }
    @Test(expected = IllegalArgumentException.class)
    public void throwsIllegalArgumentExceptionBecauseNotSameLength() throws IOException {
        String[] dnaLines = readDnaLines("/invalid-no-same-length.dna");
        assert mutantChecker.isMutant(dnaLines);
    }
    private String[] readDnaLines(String resourceName) throws IOException {
        URL resourceUrl = MutantCheckerTest.class.getResource(resourceName);
        List<String> strings = FileUtils.readLines(new File(resourceUrl.getFile()), Charset.forName("utf-8"));
        return strings.toArray(new String[strings.size()]);
    }
}
