package com.fpdecuzzi.mutant.checker.service;

import com.fpdecuzzi.mutant.checker.persistence.DnaRepository;
import com.fpdecuzzi.mutant.checker.persistence.MockDnaRepository;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.fpdecuzzi.mutant.checker.service")
@EnableCaching
public class ServiceTestConfig {
    @Bean
    public DnaRepository dnaRepository(){
        return new MockDnaRepository(100);
    }
    @Bean
    public CacheManager cacheManager(){
        return new ConcurrentMapCacheManager();
    }
}
