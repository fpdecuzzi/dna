package com.fpdecuzzi.mutant.checker.service;

import com.fpdecuzzi.mutant.checker.model.request.DnaRequest;
import com.fpdecuzzi.mutant.checker.persistence.DnaRepository;
import com.fpdecuzzi.mutant.checker.persistence.MockDnaRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest( classes = ServiceTestConfig.class)
public class MutantServiceTest {

    public static final int MAX_WAITING_TIME_FOR_SAVED_DNA = 1000;
    @Autowired
    private DnaRepository dnaRepository;
    @Autowired
    private MutantService mutantService;
    private final String[] DNA = new String[]{"ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"};
    private final String HASH = "853233798d8faabcb74a85822eee596c";
    private final String SEQUENCE = "ATGCGA,CAGTGC,TTATGT,AGAAGG,CCCCTA,TCACTG";

    @Before
    public void init() {
        dnaRepository.deleteAll();
    }

    @Test
    public void checkCorrectHashFromDna() {
        String obtainedHash = mutantService.dnaHash(DNA);
        Assert.assertEquals(HASH, obtainedHash);
    }
    @Test
    public void checkCorrectHashFromDnaIfItIsLowerCase() {
        String[] lowerCased = Arrays.asList(DNA)
                .stream()
                .map(c -> c.toLowerCase())
                .toArray(String[]::new);
        String obtainedHash = mutantService.dnaHash(lowerCased);
        Assert.assertEquals(HASH, obtainedHash);
    }

    @Test
    public void checkCorrectStringSequenceFromDna() {
        String sequence = mutantService.dnaAsStringSequence(DNA);
        Assert.assertEquals(SEQUENCE, sequence);
    }

    @Test
    public void checkingDnaNotPreviouslyStored() throws InterruptedException {
        boolean present = this.dnaRepository.findById(HASH).isPresent();
        Assert.assertFalse(HASH + "is already present", present);
        boolean isMutant = mutantService.check(new DnaRequest(DNA));
        Assert.assertTrue(HASH + " is not a mutant", isMutant);
        //Dado que el guardado del pedido en el mutant service es async, esperamos un tiempo para poder obtenerlo.
        present = this.dnaRepository.findById(HASH).isPresent();
        long startedWaitingMillis = System.currentTimeMillis();
        while (!present && System.currentTimeMillis() - startedWaitingMillis <= MAX_WAITING_TIME_FOR_SAVED_DNA){
            Thread.currentThread().sleep(10);
            present = this.dnaRepository.findById(HASH).isPresent();
        }
        Assert.assertTrue(HASH + " is not present", present);
    }

    @Test
    public void obtainDnaInCacheIsFasterThanConnectingToDataBase(){
        mutantService.check(new DnaRequest(DNA));
        long firstGetTime = System.currentTimeMillis();
        //Obtenemos un registro, como es la primera vez va a tardar más, ya que no estará en el cache.
        mutantService.getDnaByHash(HASH);
        firstGetTime = System.currentTimeMillis() - firstGetTime;
        long secondGetTime = System.currentTimeMillis();
        //en cambio, la segunda vez deberá tardar menos.
        mutantService.getDnaByHash(HASH);
        secondGetTime = System.currentTimeMillis() - secondGetTime;
        Assert.assertTrue(secondGetTime < firstGetTime);

    }


}
