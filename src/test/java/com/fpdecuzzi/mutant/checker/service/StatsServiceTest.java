package com.fpdecuzzi.mutant.checker.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fpdecuzzi.mutant.checker.model.stats.DnaChecksStat;
import com.fpdecuzzi.mutant.checker.persistence.Dna;
import com.fpdecuzzi.mutant.checker.persistence.DnaRepository;
import com.fpdecuzzi.mutant.checker.persistence.MockDnaRepository;
import com.fpdecuzzi.mutant.checker.util.ResourceUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest( classes = ServiceTestConfig.class)
public class StatsServiceTest {
    @Autowired
    private DnaRepository dnaRepository;
    @Autowired
    private StatsService statsService;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void init() {
        dnaRepository.deleteAll();
    }

    @Test
    public void checkCorrectStats() throws IOException {
        File resourceFile = ResourceUtil.getResourceFile("/dna-registers-for-stats.json");
        Set<Dna> dnas = objectMapper.readValue(resourceFile, new TypeReference<Set<Dna>>() {
        });
        dnas.forEach(dna -> dnaRepository.save(dna));
        long humansCount = dnaRepository.countHumans();
        long mutantsCount = dnaRepository.countMutants();
        BigDecimal ratio = new BigDecimal((float) mutantsCount / humansCount)
                .setScale(2, BigDecimal.ROUND_HALF_UP);
        DnaChecksStat checksStat = statsService.getChecksStat();
        Assert.assertEquals(humansCount, checksStat.getCountHumanDna());
        Assert.assertEquals(mutantsCount, checksStat.getCountMutantDna());
        Assert.assertEquals(ratio, checksStat.getRatio());
    }
}
