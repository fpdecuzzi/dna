package com.fpdecuzzi.mutant.checker.persistence;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.Mockito.mock;

public class MockDnaRepository implements DnaRepository {

    private static final Logger logger = Logger.getLogger(MockDnaRepository.class);
    private final DnaRepository dnaRepository;
    private final Map<String, Dna> dnaMap = new HashMap<>();
    private final long delay;


    public MockDnaRepository(long delay) {
        this.delay = delay;
        dnaRepository = mock(DnaRepository.class);
    }

    public MockDnaRepository(){
        this(0);
    }

    @Override
    public long countHumans() {
        return dnaMap.values().stream().filter(dna -> !dna.isMutant()).count();
    }

    @Override
    public long countMutants() {
        return dnaMap.values().stream().filter(dna -> dna.isMutant()).count();
    }

    @Override
    public <S extends Dna> S save(S entity) {
        dnaMap.put(entity.getHash(), entity);
        return entity;
    }

    @Override
    public <S extends Dna> Iterable<S> saveAll(Iterable<S> entities) {
        entities.forEach(entity -> save(entity));
        return entities;
    }

    @Override
    public Optional<Dna> findById(String s) {
        addDelay();
        return Optional.ofNullable(dnaMap.get(s));
    }

    private void addDelay() {
        if (this.delay > 0){
            try {
                Thread.currentThread().sleep(delay);
            } catch (InterruptedException e) {
            }
        }
    }

    @Override
    public boolean existsById(String s) {
        return dnaMap.containsKey(s);
    }

    @Override
    public Iterable<Dna> findAll() {
        return () -> dnaMap.values().stream().iterator();
    }

    @Override
    public Iterable<Dna> findAllById(Iterable<String> strings) {
        return () -> dnaMap.values().stream().filter(dna -> Stream.of(strings).anyMatch(string -> dna.getHash().equals(string))).iterator();

    }

    @Override
    public long count() {
        return dnaMap.values().stream().count();
    }

    @Override
    public void deleteById(String s) {
        dnaMap.remove(s);
    }

    @Override
    public void delete(Dna entity) {
        deleteById(entity.getHash());
    }

    @Override
    public void deleteAll(Iterable<? extends Dna> entities) {
        entities.forEach(entity -> deleteById(entity.getHash()));
    }

    @Override
    public void deleteAll() {

    }
}
