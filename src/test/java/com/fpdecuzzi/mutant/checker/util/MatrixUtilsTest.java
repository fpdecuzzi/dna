package com.fpdecuzzi.mutant.checker.util;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class MatrixUtilsTest {

    private static Logger logger = Logger.getLogger(MatrixUtilsTest.class);

    private final Map<String, Character[][]> matrixMap = new HashMap<>();
    private final Map<String, Character[][]> matrixDiagonalsLeftToRight = new HashMap<>();
    private final Map<String, Character[][]> matrixDiagonalRightToLeft = new HashMap<>();

    public MatrixUtilsTest() throws IOException {

        List<String> identifiers = readLines("/index.matrix");
        for (String identifier : identifiers) {
            String filePrefix = "/" + identifier;
            matrixDiagonalsLeftToRight.put(identifier, charMatrixfromResource(filePrefix + "-left-to-right-diagonals.matrix"));
            matrixDiagonalRightToLeft.put(identifier, charMatrixfromResource(filePrefix + "-right-to-left-diagonals.matrix"));
            matrixMap.put(identifier, charMatrixfromResource(filePrefix + ".matrix"));
        }

    }

    private static Character[][] charMatrixfromResource(String resourceName) throws IOException {
        List<String> strings = readLines(resourceName);
        int size = strings.size();
        Character[][] characterMatrix = new Character[size][];
        IntStream.range(0, size).forEach(i -> {
            int length = strings.get(i).length();
            characterMatrix[i] = new Character[length];
            Character[] characterArray = characterMatrix[i];
            IntStream.range(0, length).forEach(j -> {
                characterArray[j] = strings.get(i).charAt(j);
            });
        });
        return characterMatrix;
    }

    private static List<String> readLines(String resourceName) throws IOException {
        URL resourceUrl = MatrixUtilsTest.class.getResource(resourceName);
        return FileUtils.readLines(new File(resourceUrl.getFile()), Charset.forName("utf-8"));
    }

    @Test
    public void transposedMatrixIsOriginalMatrixRowsByColumns() {
        for (String key : matrixMap.keySet()) {
            logger.info("transposedMatrixIsOriginalMatrixRowsByColumns " + key);
            assertTransposeMatrix(matrixMap.get(key));
        }
    }

    @Test
    public void diagonalsLeftToRight() {
        for (String key : matrixMap.keySet()) {
            logger.info("diagonalsLeftToRight " + key);
            assertDiagonalsLeftToRight(matrixMap.get(key), matrixDiagonalsLeftToRight.get(key));
        }
    }

    @Test
    public void diagonalsRightToLeft() {
        for (String key : matrixMap.keySet()) {
            logger.info("diagonalsRightToLeft " + key);
            assertDiagonalsRightToLeft(matrixMap.get(key), matrixDiagonalRightToLeft.get(key));
        }
    }

    private void assertTransposeMatrix(Character[][] matrix) {
        logger.info("matrix: \n"+MatrixUtils.print(matrix));
        logger.info("#################################");
        Character[][] transposed = MatrixUtils.transpose(Character.class, matrix);
        logger.info("transposed: \n"+MatrixUtils.print(transposed));
        logger.info("#################################");
        Character[][] reTransposed = MatrixUtils.transpose(Character.class, transposed);
        for (int i = 0; i < reTransposed.length; i++) {
            Character[] row = reTransposed[i];
            for (int j = 0; j < row.length; j++) {
                assert reTransposed[i][j] == matrix[i][j];
            }
        }
    }

    private void assertDiagonalsLeftToRight(Character[][] matrix, Character[][] diagonals) {
        Character[][] obtainedDiagonals = MatrixUtils.getDiagonals(Character.class, matrix);
        assertDiagonals(matrix, diagonals, obtainedDiagonals);
    }

    private void assertDiagonalsRightToLeft(Character[][] matrix, Character[][] diagonals) {
        Character[][] transposeReversed = MatrixUtils.transposeReversed(Character.class, matrix);
        Character[][] obtainedDiagonals = MatrixUtils.getDiagonals(Character.class, transposeReversed);
        assertDiagonals(matrix, diagonals, obtainedDiagonals);
    }

    private void assertDiagonals(Character[][] matrix, Character[][] diagonals, Character[][] obtainedDiagonals) {
        assert obtainedDiagonals.length == (((matrix.length - 1) * 2) + 1);
        logger.info("matrix: \n" + MatrixUtils.print(matrix));
        logger.info("#################################");
        logger.info("diagonals: \n" +MatrixUtils.print(diagonals));
        logger.info("#################################");
        logger.info("obtainedDiagonals: \n" +MatrixUtils.print(obtainedDiagonals));
        logger.info("#################################");
        for (int i = 0; i < diagonals.length; i++) {
            Character[] charArray = diagonals[i];
            Character[] obtainedDiagonal = obtainedDiagonals[i];
            assert charArray.length == obtainedDiagonal.length;
            for (int j = 0; j < charArray.length; j++) {
                assert charArray[j] == obtainedDiagonal[j];
            }
        }
    }

}
