package com.fpdecuzzi.mutant.checker.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fpdecuzzi.mutant.checker.model.stats.DnaChecksStat;
import com.fpdecuzzi.mutant.checker.persistence.DnaRepository;
import com.fpdecuzzi.mutant.checker.persistence.MockDnaRepository;
import com.fpdecuzzi.mutant.checker.service.StatsService;
import org.junit.Assert;
import org.junit.Test;

public class StatsControllerTest {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private DnaRepository dnaRepository = new MockDnaRepository();
    private StatsController statsController = new StatsController(new StatsService(dnaRepository));

    @Test
    public void checkCorrectStatsResponseSerialization() throws JsonProcessingException {
        DnaChecksStat checksStat = statsController.stats();
        String json = objectMapper.writeValueAsString(checksStat);
        String expectedJson = String.format("{\"count_mutant_dna\":%s,\"count_human_dna\":%s,\"ratio\":%s}"
                , checksStat.getCountMutantDna()
                , checksStat.getCountHumanDna()
                , checksStat.getRatio());
        Assert.assertEquals(expectedJson, json);
    }

}
