package com.fpdecuzzi.mutant.checker.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fpdecuzzi.mutant.checker.model.request.DnaRequest;
import com.fpdecuzzi.mutant.checker.persistence.DnaRepository;
import com.fpdecuzzi.mutant.checker.persistence.MockDnaRepository;
import com.fpdecuzzi.mutant.checker.service.MutantService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

import static com.fpdecuzzi.mutant.checker.util.ResourceUtil.getResourceFile;

public class MutantControllerTest {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private DnaRepository dnaRepository = new MockDnaRepository();
    private MutantController mutantController = new MutantController(new MutantService(dnaRepository));

    @Test
    public void isMutantByRequestWithConsecutiveADiagonal() throws IOException {
        ResponseEntity responseEntity = obtainResponseEntityFromDnaRequestResource("/dna-request-consecutive-a-diagonal.json");
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void isMutantByRequestWithConsecutiveCHorizontal() throws IOException {
        ResponseEntity responseEntity = obtainResponseEntityFromDnaRequestResource("/dna-request-consecutive-c-horizontal.json");
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void isMutantByRequestWithConsecutiveGVertical() throws IOException {
        ResponseEntity responseEntity = obtainResponseEntityFromDnaRequestResource("/dna-request-consecutive-g-vertical.json");
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void notIsMutantByValidRequest() throws IOException {
        ResponseEntity responseEntity = obtainResponseEntityFromDnaRequestResource("/dna-request-no-mutant.json");
        Assert.assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
    }

    @Test
    public void notIsMutantByNullDnaRequest() throws IOException {
        ResponseEntity responseEntity = obtainResponseEntityFromDnaRequestResource("/dna-request-null.json");
        Assert.assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
    }

    @Test
    public void notIsMutantByEmptyDnaValidRequest() throws IOException {
        ResponseEntity responseEntity = obtainResponseEntityFromDnaRequestResource("/dna-request-empty.json");
        Assert.assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
    }

    @Test
    public void notIsMutantByNotSquareDnaRequest() throws IOException {
        ResponseEntity responseEntity = obtainResponseEntityFromDnaRequestResource("/dna-request-no-square.json");
        Assert.assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
    }


    private ResponseEntity obtainResponseEntityFromDnaRequestResource(String resourceName) throws IOException {
        DnaRequest dnaRequest = objectMapper.readValue(getResourceFile(resourceName), DnaRequest.class);
        return mutantController.check(dnaRequest);
    }
}
