import com.fasterxml.jackson.databind.ObjectMapper;
import com.fpdecuzzi.mutant.checker.model.request.DnaRequest;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Generador de DNA aleatorio y pedidos contra el servicio especificado.
 */
public class DnaLoader {

    static final char[] BASES = new char[]{'A', 'C', 'T', 'G'};

    /**
     * Genera DNAs aleatorios y realiza los correspondientes chequeos contra el servicio especificado.
     * @param args args[0] int dimensión de la matriz de DNA a generar, default 6.
     *             args[1] int cantidad de secuencias de DNA a generar y chequear, default 10000.
     *             args[2] String url base del servicio, default https://fpdecuzzi-mutant.appspot.com.
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {

        int n = args.length > 0 ? Integer.valueOf(args[0]) : 6;
        int times = args.length > 1 ? Integer.valueOf(args[1]) : 10000;
        String url = args.length > 2 ? args[2] : "https://fpdecuzzi-mutant.appspot.com";
        Set<Pair<Integer,String>> responses = new HashSet<>();
        CountDownLatch countDownLatch = new CountDownLatch(times);
        for (int i = 0; i < times; i++) {
            new Thread(() -> {
                String[] dna = randomDna(n);
                DnaRequest dnaRequest = new DnaRequest(dna);
                try {
                    CloseableHttpResponse closeableHttpResponse = requestDnaCheck(url, dnaRequest);
                    StatusLine statusLine = closeableHttpResponse.getStatusLine();
                    responses.add(new ImmutablePair<>(statusLine.getStatusCode(), statusLine.getReasonPhrase()));
                } catch (IOException e) {
                    e.printStackTrace();
                    responses.add(new ImmutablePair<>(-1,e.getLocalizedMessage()));
                }finally {
                    countDownLatch.countDown();
                }

            }).start();
        }

        countDownLatch.await();

    }

    private static String[] randomDna(int n) {
        String[] dna = new String[n];
        StringBuffer stringBuffer = new StringBuffer();
        int baseIndex;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                baseIndex = ThreadLocalRandom.current().nextInt(0, 4);
                stringBuffer.append(BASES[baseIndex]);
            }
            dna[i] = stringBuffer.toString();
            stringBuffer = new StringBuffer();
        }
        return dna;
    }

    private static CloseableHttpResponse requestDnaCheck(String url, DnaRequest dnaRequest) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url + "/mutant");


        String json = new ObjectMapper().writeValueAsString(dnaRequest);
        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");

        CloseableHttpResponse response = client.execute(httpPost);
        client.close();
        return response;
    }
}
