package com.fpdecuzzi.mutant.checker.service;

import com.fpdecuzzi.mutant.checker.model.MutantChecker;
import com.fpdecuzzi.mutant.checker.model.request.DnaRequest;
import com.fpdecuzzi.mutant.checker.persistence.Dna;
import com.fpdecuzzi.mutant.checker.persistence.DnaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
/**
 * Clase que expone métodos para el manejo de chequeos de DNA
 */
public class MutantService {

    private final DnaRepository dnaRepository;
    private ExecutorService saveDnaExecutorService = Executors.newFixedThreadPool(10);

    @Autowired
    public MutantService(DnaRepository dnaRepository) {
        this.dnaRepository = dnaRepository;
    }

    /**
     * Método para chequear si un pedido de
     * @param dnaRequest pedido de chequeo de DNA.
     * @return si la secuencia analizada pertenece a un mutante.
     * @throws IllegalArgumentException
     */
    public boolean check(DnaRequest dnaRequest) throws IllegalArgumentException {
        String[] dna = dnaRequest.getDna();
        String hash = dnaHash(dna);
        Optional<Dna> optionalDna = getDnaByHash(hash);
        if (optionalDna.isPresent()) {
            return optionalDna.get().isMutant();
        }
        boolean isMutant = MutantChecker.isMutant(dna);
        String sequence = dnaAsStringSequence(dna);
        Dna entity = new Dna(hash, sequence, isMutant);
        //Guardamos la entidad en un thread aparte, ya que lo importante es devolver rápidamente si es o no un mutante primero
        save(entity);
        return isMutant;
    }

    /**
     * Método que obtiene el hash MD5 correspondiente a una secuencia de DNA.
     * @param dna array de secuencias de DNA.
     * @return hash MD5
     */
    public String dnaHash(String[] dna) {
        String sequence = dnaAsStringSequence(dna);
        String hash = DigestUtils.md5DigestAsHex(sequence.getBytes());
        return hash;
    }

    /**
     * Método que transforma un array con secuencias de DNA en un String.
     * @param dna array de secuencias de DNA.
     * @return secuencias de DNA en un String.
     */
    public String dnaAsStringSequence(String[] dna) {
        return Arrays.asList(dna)
                .stream()
                .map(s -> s.toUpperCase())
                .collect(Collectors.joining(","));
    }

    /**
     * Método que se conecta a la base de datos para obtener el DNA asociado a un hash.
     * Se utiliza un cache para evitar conectarse si un valor ya fue pedido anteriormente,
     * el mismo utiliza una política de remoción de elemento LRU.
     * @param hash representación hash del DNA a buscar.
     * @return
     */
    @Cacheable("dnas")
    public Optional<Dna> getDnaByHash(String hash) {
        return dnaRepository.findById(hash);
    }
    private void save(Dna entity) {
        saveDnaExecutorService.execute(() -> dnaRepository.save(entity));
    }
}
