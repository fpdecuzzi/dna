package com.fpdecuzzi.mutant.checker.service;

import com.fpdecuzzi.mutant.checker.model.stats.DnaChecksStat;
import com.fpdecuzzi.mutant.checker.persistence.DnaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Clase que expone métodos relacionados con estadística de DNA.
 */
@Service
public class StatsService {

    private final DnaRepository dnaRepository;

    @Autowired
    public StatsService(DnaRepository dnaRepository) {
        this.dnaRepository = dnaRepository;
    }

    /**
     * Método para obtener estadísticas de los análisis de DNA realizados hasta el momento.
     * @return un objeto que contiene cantidad de mutantes, cantidad de humanos y su correspondiente ratio
     */
    public DnaChecksStat getChecksStat() {
        long humansCount = dnaRepository.countHumans();
        long mutantsCount = dnaRepository.countMutants();
        float val = humansCount != 0 ? (float) mutantsCount / humansCount : 0;
        BigDecimal ratio = new BigDecimal(val)
                           .setScale(2, BigDecimal.ROUND_HALF_UP);
        return new DnaChecksStat(mutantsCount, humansCount, ratio);
    }
}
