package com.fpdecuzzi.mutant.checker.persistence;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DnaRepository extends CrudRepository<Dna, String> {
    @AllowFiltering
    @Query("SELECT count(hash) from Dna where mutant = false")
    long countHumans();

    @AllowFiltering
    @Query("SELECT count(hash) from Dna where mutant = true")
    long countMutants();
}
