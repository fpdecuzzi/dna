package com.fpdecuzzi.mutant.checker.persistence;

import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("dna")
public class Dna {
    @PrimaryKey
    private String hash;
    private String sequence;
    @Indexed
    private boolean mutant;

    public Dna() {
    }

    public Dna(String hash, String sequence, boolean mutant) {
        this.hash = hash;
        this.sequence = sequence;
        this.mutant = mutant;
    }

    public String getHash() {
        return hash;
    }

    public String getSequence() {
        return sequence;
    }

    public boolean isMutant() {
        return mutant;
    }
}
