package com.fpdecuzzi.mutant.checker.model.dna;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;

import static com.fpdecuzzi.mutant.checker.util.MatrixUtils.*;

/***
 * Clase que representa una matriz de bases de dna,
 * la misma debe ser cuadrada de NxN
 */
public class DnaMatrix {


    private final DnaBase[][] matrix;
    private final int length;

    /**
     * Constructor de una matriz de bases de dna en base a un arreglo de dos dimensiones de bases.
     * @param matrix array de dos dimensiones con las bases de dna para la matriz.
     * @throws IllegalArgumentException si el alto y ancho de la matriz no es igual.
     */
    public DnaMatrix(DnaBase[][] matrix) throws IllegalArgumentException {
        this.matrix = matrix;
        this.length = matrix.length;
        for (int i = 0; i < length; i++) {
            if (matrix[i].length != length) {
                throw new IllegalArgumentException("array's height must be equal to width");
            }
        }
    }

    /***
     * Método que permite construir una matriz de bases de dna a partir de un array representativo de las mismas
     * @param sequences array que representa secuencias de bases de dna.
     * @return
     * @throws IllegalArgumentException si alguno de los caracteres de las cadenas no es una base dna.
     */
    public static DnaMatrix from(String[] sequences) throws IllegalArgumentException {
        DnaBase[][] matrix = DnaBase.from(sequences);
        return new DnaMatrix(matrix);
    }

    /***
     *
     * @return el alto y ancho de la matriz.
     */
    private int getLength() {
        return length;
    }

    /***
     * Método que permite obtener Iterados para recorrer las secuencias de dna en distintas direcciones.
     * @param direction dirección en la cual se quiere recorrer las secuencias de dna.
     * @return
     */
    public Iterator<Iterator<DnaBase>> getIterator(DnaMatrixDirection direction) {
        DnaBase[][] toIterate;
        switch (direction) {
            case Horizontal:
                toIterate = matrix;
                break;
            case Vertical:
                toIterate = transpose(DnaBase.class, matrix);
                break;
            default:
                //Obtenemos las diagonales en todas la direcciones.
                toIterate = getDiagonals(DnaBase.class, matrix);
                DnaBase[][] transposeReversed = transposeReversed(DnaBase.class, matrix);
                toIterate = ArrayUtils.addAll(toIterate, getDiagonals(DnaBase.class, transposeReversed));
                break;
        }
        LinkedHashSet<Iterator<DnaBase>> linkedHashSet = new LinkedHashSet<>(toIterate.length);
        for (int i = 0; i < toIterate.length; i++) {
            linkedHashSet.add(Arrays.stream(toIterate[i]).iterator());
        }
        return linkedHashSet.iterator();
    }


}
