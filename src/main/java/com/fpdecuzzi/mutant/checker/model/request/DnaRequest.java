package com.fpdecuzzi.mutant.checker.model.request;

/**
 * Representa un pedido de análisis de ADN.
 */
public class DnaRequest {
    private String[] dna;
    public DnaRequest() {
    }
    public DnaRequest(String[] dna) {
        this.dna = dna;
    }
    public void setDna(String[] dna) {
        this.dna = dna;
    }
    public String[] getDna() {
        return dna;
    }
}
