package com.fpdecuzzi.mutant.checker.model;

import com.fpdecuzzi.mutant.checker.model.dna.DnaBase;
import com.fpdecuzzi.mutant.checker.model.dna.DnaMatrix;
import com.fpdecuzzi.mutant.checker.model.dna.DnaMatrixDirection;

import java.util.Iterator;
import java.util.Optional;

/***
 * Clase que expone chequeo de secuencia de ADN para saber si pertenece a un mutante o no.
 */
public class MutantChecker {

    /***
     * Cantidad de veces que debe repetirse una base para considerarse mutante
     */
    private static final int CONSECUTIVE_TIMES_TO_BE_MUTANT = 4;

    /***
     * Método para analizar si una secuencia de dna pertenece a un mutante
     * @param dna secuencia de dna a analizar
     * @return
     * @throws IllegalArgumentException si la secuencia de dna no contiene una base válida, si no todas las secuencias tienen
     * el mismo largo o si el largo de las secuencias es distinto a la cantidad de las mismas.
     */
    public static boolean isMutant(String[] dna) throws IllegalArgumentException {
        DnaMatrix matrix = DnaMatrix.from(dna);
        return checkHasConsecutiveDnaBase(matrix.getIterator(DnaMatrixDirection.Horizontal)) ||
                checkHasConsecutiveDnaBase(matrix.getIterator(DnaMatrixDirection.Vertical)) ||
                checkHasConsecutiveDnaBase(matrix.getIterator(DnaMatrixDirection.Diagonal));
    }

    /***
     * Método que chequea si una secuencia de bases supera o iguala la cantidad seguida de bases
     * @param iterators de donde se buscaran secuencias base consecutivas
     * @return
     */
    private static boolean checkHasConsecutiveDnaBase(Iterator<Iterator<DnaBase>> iterators) {
        Optional<DnaBase> optLastBase = Optional.empty();
        int consecutiveCount = 0;
        DnaBase base;
        while (iterators.hasNext()) {
            for (Iterator<DnaBase> iterator = iterators.next(); iterator.hasNext(); ) {
                base = iterator.next();
                consecutiveCount = optLastBase.isPresent() && optLastBase.get().equals(base) ? consecutiveCount + 1 : 1;
                if (consecutiveCount >= CONSECUTIVE_TIMES_TO_BE_MUTANT) {
                    return true;
                }
                optLastBase = Optional.of(base);
            }
            optLastBase = Optional.empty();
            consecutiveCount = 0;
        }
        return false;
    }
}
