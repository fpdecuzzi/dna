package com.fpdecuzzi.mutant.checker.model.dna;

/**
 * Representación de los distintas direcciones en las cuales se puede recorrer una matriz de dna
 */
public enum DnaMatrixDirection {
    Vertical, Horizontal, Diagonal
}
