package com.fpdecuzzi.mutant.checker.model.dna;

import java.util.Arrays;

/***
 * Respresenta una base de DNA
 */
public enum DnaBase {

    /*Adenina*/
    A,
    /*Tamina*/
    T,
    /*Citosina*/
    C,
    /*Guanina*/
    G;

    /***
     * Métood que devuelve una base de dna a partir de un caracter.
     * @param c caracter que quiere tansformarse en una base de DNA.
     * @return
     * @throws IllegalArgumentException
     */
    public static DnaBase from(char c) throws IllegalArgumentException {
        return Arrays.stream(DnaBase.values())
                .filter(base -> base.name().equalsIgnoreCase(String.valueOf(c)))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(c + " is not a valid base"));
    }

    /***
     * Método que retorna un array de una dimensión de bases de dna en base a una secuencia de caracteres.
     * @param sequence secuencia de caracteres de la cual se quiere obtener bases de dna.
     * @return
     * @throws IllegalArgumentException
     */
    public static DnaBase[] from(char[] sequence) throws IllegalArgumentException {
        DnaBase[] bases = new DnaBase[sequence.length];
        for (int i = 0; i < sequence.length; i++) {
            bases[i] = DnaBase.from(sequence[i]);
        }
        return bases;
    }

    /***
     * Método que retorna un array de dos dimensiones de bases de dna en base a una secuencia de caracteres.
     * @param sequences secuencias de caracteres de la cual se quiere obtener bases de dna.
     * @return
     * @throws IllegalArgumentException
     */
    public static DnaBase[][] from(String[] sequences) throws IllegalArgumentException {
        DnaBase[][] matrix = new DnaBase[sequences.length][sequences[0].length()];
        for (int i = 0; i < sequences.length; i++) {
            matrix[i] = DnaBase.from(sequences[i].toCharArray());
        }
        return matrix;
    }

}
