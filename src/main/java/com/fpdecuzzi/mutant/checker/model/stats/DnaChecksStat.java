package com.fpdecuzzi.mutant.checker.model.stats;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

/**
 * Representa una respuesta a pedido de estadísticas de análisis de ADN.
 */
@JsonPropertyOrder({ "count_mutant_dna", "count_human_dna", "ratio" })
public class DnaChecksStat {
    @JsonProperty("count_mutant_dna")
    private final long countMutantDna;
    @JsonProperty("count_human_dna")
    private final long countHumanDna;
    private final BigDecimal ratio;

    public DnaChecksStat(long countMutantDna, long countHumanDna, BigDecimal ratio) {
        this.countMutantDna = countMutantDna;
        this.countHumanDna = countHumanDna;
        this.ratio = ratio;
    }

    public long getCountMutantDna() {
        return countMutantDna;
    }

    public long getCountHumanDna() {
        return countHumanDna;
    }

    public BigDecimal getRatio() {
        return ratio;
    }
}
