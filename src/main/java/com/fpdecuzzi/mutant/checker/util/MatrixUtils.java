package com.fpdecuzzi.mutant.checker.util;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Clase que expone métodos útiles para el manejo de matrices.
 */
public class MatrixUtils {
    /**
     * Método que permite obtener las diagonales de un array de dos dimensiones, de izquierda a derecha
     * Nro Diag = ((N -1) * 2) + 1
     *
     * @param clazz clase de objetos que contiene el array
     * @param array array de dos dimensiones del cual se quiere obtener las diagonales.
     * @param <T>   tipo de dato del array
     * @return las diagonales de un array de dos dimensiones
     */
    public static <T> T[][] getDiagonals(Class<T> clazz, T[][] array) {
        int dim = array.length;
        T[][] diagonal = (T[][]) Array.newInstance(clazz, dim * 2 - 1, dim);
        for (int k = 0; k < dim * 2; k++) {
            for (int j = 0; j <= k; j++) {
                int i = k - j;
                if (i < dim && j < dim) {
                    diagonal[k][i] = array[i][j];
                }
            }
        }
        return removeNulls(clazz, diagonal);
    }


    /**
     * Método que permite remover nulos de un array de dos dimensiones.
     *
     * @param clazz clase de objetos que contiene el array.
     * @param array array de dos dimensiones del cual se quiere eliminar nulos.
     * @param <T>   tipo de dato del array.
     * @return array de dos dimensiones sin nulos.
     */
    public static <T> T[][] removeNulls(Class<T> clazz, T[][] array) {
        return Arrays.stream(array)
                .filter(val -> val != null)
                .map(val -> Arrays.asList(val)
                        .stream()
                        .filter(s -> s != null)
                        .toArray(size -> (T[]) Array.newInstance(clazz, size))
                ).toArray(size -> (T[][]) Array.newInstance(clazz, size, size));
    }

    /***
     * Método que traspone un array de dos dimensiones.
     * @param clazz clase de objetos que contiene el array.
     * @param array array de dos dimensiones del cual se quiere eliminar nulos.
     * @param <T> tipo de dato del array.
     * @return array de dos dimensiones traspuesto.
     */
    public static <T> T[][] transpose(Class<T> clazz, T[][] array) {
        int length = array.length;
        T[][] transposed = (T[][]) Array.newInstance(clazz, length, length);
        for (int row = 0; row < length; ++row) {
            for (int col = 0; col < length; ++col) {
                transposed[col][row] = array[row][col];
            }
        }
        return transposed;
    }

    /**
     * Método que traspone un array de dos dimensiones y luego invierte el orden.
     *
     * @param clazz clase de objetos que contiene el array.
     * @param array array de dos dimensiones del cual se quiere eliminar nulos.
     * @param <T>   tipo de dato del array.
     * @return array de dos dimensiones traspuesto y con el orden de filas invertido.
     */
    public static <T> T[][] transposeReversed(Class<T> clazz, T[][] array) {
        int length = array.length;
        T[][] transposed = transpose(clazz, array);
        T[][] transposedReverse = (T[][]) Array.newInstance(clazz, length, length);
        for (int i = 0; i < length; i++) {
            transposedReverse[length - 1 - i] = transposed[i];
        }
        return transposedReverse;
    }

    /**
     * /**
     * Método que imprime un array de dos dimesiones.
     *
     * @param arrays arrays de dos dimensiones.
     *@return String con la matriz impresa.
     */
    public static <T> String print(T[][] arrays) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('\n');
        int length = arrays.length;
        for (int i = 0; i < length; i++) {
            T[] array = arrays[i];
            for (int j = 0; j < array.length; j++) {
                if (array != null && j < array.length) {
                    stringBuffer.append(array[j] + " ");
                } else {
                    stringBuffer.append("null");
                }
            }
            stringBuffer.append('\n');
        }
        return stringBuffer.toString();
    }
}
