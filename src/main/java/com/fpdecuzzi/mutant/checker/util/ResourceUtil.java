package com.fpdecuzzi.mutant.checker.util;

import java.io.File;

/**
 * Clase con métodos para manejar recursos internos de la aplicación.
 */
public class ResourceUtil {
    public static File getResourceFile(String resourceName) {
        return new File(ResourceUtil.class.getResource(resourceName).getFile());
    }
}
