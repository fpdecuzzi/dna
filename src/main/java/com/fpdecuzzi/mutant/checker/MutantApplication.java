package com.fpdecuzzi.mutant.checker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@SpringBootApplication
@EnableSwagger2
@EnableCaching
public class MutantApplication {
    @Configuration
    public class SwaggerConfig {
        @Bean
        public String appVersion() {
            return MutantApplication.class.getPackage().getImplementationVersion();
        }

        @Bean
        public Docket api() {
            return new Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                    .paths(PathSelectors.any())
                    .build()
                    .apiInfo(apiInfo());
        }

        private ApiInfo apiInfo() {
            return new ApiInfo(
                    "Mutant Checker Rest API",
                    "Permite consultar si una secuencia de ADN pertence a un mutante y consultar estadísticas",
                    appVersion(),
                    "",
                    new Contact("Francisco Pablo Decuzzi", "https://www.linkedin.com/in/francisco-pablo-decuzzi-8a9347160/", "fpdecuzzi@gmail.com"),
                    "", "", Collections.emptyList());
        }

    }

    public static void main(String[] args) {
        SpringApplication.run(MutantApplication.class, args);
    }
}
