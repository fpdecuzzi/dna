package com.fpdecuzzi.mutant.checker.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class MainController {
    @RequestMapping(method = RequestMethod.GET)
    public String base() {
        return "redirect:/swagger-ui.html";
    }

    @RequestMapping(value = "/_ah/health", method = RequestMethod.GET)
    public ResponseEntity<String> healthCheck() {
        return new ResponseEntity<>("working", HttpStatus.OK);
    }

}
