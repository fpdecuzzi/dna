package com.fpdecuzzi.mutant.checker.controller;

import com.fpdecuzzi.mutant.checker.model.stats.DnaChecksStat;
import com.fpdecuzzi.mutant.checker.service.StatsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stats")
public class StatsController {

    private final StatsService statsService;

    @Autowired
    public StatsController(StatsService statsService) {
        this.statsService = statsService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public DnaChecksStat stats() {
        DnaChecksStat checksStat = statsService.getChecksStat();
        return checksStat;
    }

}
