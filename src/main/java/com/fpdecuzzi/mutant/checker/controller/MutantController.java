package com.fpdecuzzi.mutant.checker.controller;

import com.fpdecuzzi.mutant.checker.model.request.DnaRequest;
import com.fpdecuzzi.mutant.checker.service.MutantService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mutant")
public class MutantController {

    private static Logger logger = Logger.getLogger(MutantController.class);
    private final MutantService mutantService;

    @Autowired
    public MutantController(MutantService mutantService) {
        this.mutantService = mutantService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity check(@RequestBody DnaRequest dnaRequest) {
        boolean isMutant = false;
        try {
            isMutant = this.mutantService.check(dnaRequest);
        } catch (Exception e) {
            logger.warn("check", e);
        }
        HttpStatus httpStatus = isMutant ? HttpStatus.OK : HttpStatus.FORBIDDEN;
        return new ResponseEntity(httpStatus);
    }
}
