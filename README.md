# Mutant DNA checker
Este proyecto expone una API Rest con funcionalidades de chequeo de matrices cuadradas de ADN para permitir detectar si las mismas son o no de un mutante. Además cuenta también con un método para consultar las estadísticas de los ADNs analizados.

#### Pre-requisitos

- Java 8
- Maven
- Docker
- Docker-Compose
- Google App Engine (si se quiere subir a la nube)

#### Descargar dependencias y compilar

```
mvn clean install
```
#### Correr localmente

Levantar el cluster de Cassandra
```
cd cassandra-cluster
docker-compose up
```
Modificar el application.properties que se encuentra en local-config con las ips de cluster local
```
spring.data.cassandra.contact-points= ips separadas por comas
```

iniciar el servicio
```
mvn spring-boot:run -Dspring.config.location=local-config/application.properties
```
#### Probar la solución que se encuentra en la nube

```
https://fpdecuzzi-mutant.appspot.com
```
La documentación de los métodos expuestos se encuentra en

```
https://fpdecuzzi-mutant.appspot.com/swagger-ui.html
```
#### Publicar en Google App Engine
```
mvn appengine:deploy   
```

#### Herramientas utilizadas

* [Spring Boot](https://projects.spring.io/spring-boot/) - Web Framework
* [Maven](https://maven.apache.org/) - Manejo de dependencias e integración con Google App Engine
* [Google App Engine](https://cloud.google.com/appengine/?hl=es) - Para correr el proyecto en la nube
* [Docker](https://www.docker.com/) - Para levantar instancias de Cassandra
* [Docker-Compose](https://docs.docker.com/compose/) - Para el armado de cluster de Cassandra

#### Autor

* **Francisco Pablo Decuzzi** 

